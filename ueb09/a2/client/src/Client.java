import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

import javax.sound.sampled.*;
/**
 * Client Class to play Audio send from server
 * and live chat with other clients
 * @author niels
 *
 */
public class Client {
	/**
	 * 
	 */
    public Client() {
    	// connect client to server
        try {
                Socket audioSocket = new Socket("localhost", 11433);
                Socket chatSocket = new Socket("localhost", 11434);
        	    // start recieving and playing sound
        		Sound sound = new Sound(audioSocket);
        		sound.start();
        		// start to chat with other clients
        		Chat chat = new Chat(chatSocket);
        		// ends this thread when main thread is ended
        		chat.setDaemon(true);
        		chat.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * Handles live-chat
     * @author niels
     *
     */
    class Chat extends Thread {

        Socket socket;
        BufferedReader fromServer;
        PrintStream toServer;
        Scanner keyboard;
        LinkedBlockingQueue<String> messages;
        boolean stopped = false;

        Chat(Socket s) {
            socket = s;
            try {
                toServer = new PrintStream(socket.getOutputStream());
                fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                keyboard = new Scanner(System.in);
                messages = new LinkedBlockingQueue<String>();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            /**
             * Independet thread to read messages, send from server
             */
            Thread readServerInput = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            String message = fromServer.readLine();
                            messages.put(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            // ends with main thread
            readServerInput.setDaemon(true);
            readServerInput.start();
            
            /**
             * read input from stdin 
             * and send it to server 
             */
            Thread write = new Thread() {
                public void run() {
                    while(true) {
                        try {
                            String message = keyboard.nextLine();
                            if (message.equalsIgnoreCase("stop client")){
                                System.out.println("stopping..:");
                                stopped = true;
                                break;
                            }
                            toServer.println(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            // ends with main thread
            write.setDaemon(true);
            write.start();
        }
        
        /**
         * print messages that came from server to stdout
         */
        public void run() {

            while (true) {
                try {
                    String message = messages.take();
                    System.out.println("Message recieved: " + message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    /**
     * Reads bytes incomming from server 
     * plays sound  
     * @author niels
     *
     */
    static class Sound extends Thread {

        Socket socket;
        PrintStream toServer;
        InputStream fromServer;
        // BufferedReader serverIn = new BufferedReader(
        // new InputStreamReader(socket.getInputStream()));
        Scanner keyboard;
        
        /**
         * initializes gloabal variables
         * @param s audiosocket
         */
        Sound(Socket s) {
            socket = s;
            try {
                toServer = new PrintStream(socket.getOutputStream());
                fromServer = new BufferedInputStream(socket.getInputStream());
                keyboard = new Scanner(System.in);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        
        /**
         * reads bytes from server and puts to audio output
         */
        public void run() {
            try {
            	// input from server
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(fromServer);
                // get format of audio file
                DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioInputStream.getFormat());
                // audio output
                SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
                // bytes from audiofile
                byte[] audiobytes = new byte[1024];
                int numBytesRead = 0;
                // opens line to audio output
                sourceLine.open();
                sourceLine.start();
                String message;
                while (true) {
                	// play bytes send from server
                    while ((numBytesRead = audioInputStream.read(audiobytes)) != -1) {
                        sourceLine.write(audiobytes, 0, audiobytes.length);
                    }
                    // read message from keyboard
                    message = keyboard.nextLine();
                    if (message.equalsIgnoreCase("stop")) {
                        // stop client
                    	toServer.println("stop");
                        break;
                    } else {
                        toServer.println("weiter");
                    }
                }
                // close all connections
                sourceLine.drain();
                sourceLine.stop();
                sourceLine.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            keyboard.close();
            toServer.close();
        }

    }
}
