package server;
/**
 * Class to start Server 
 * @author niels
 *
 */
public class Main {

    public static void main(String[] args) {
        Thread t = new Thread() {
            public void run() {
                Server server = new Server();
                try {
                    server.runServer(); 
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        t.start();

    }
}
