package server;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * WebRadio and LiveChat Server
 */
public class Server {
	// list of connected clients, necessary to send message to every client
    public ArrayList<ConnectedClients> clientList;
    // stores messages from client for a short time
    public LinkedBlockingQueue<String> messages;

    /**
     * initalizes global variables
     */
    public Server() {

        clientList = new ArrayList<ConnectedClients>();
        messages = new LinkedBlockingQueue<String>();
        
    }
    /**
     * Let Clients connect to Server,
     * starts thread to send audio
     * and for chat of the clients 
     * @throws IOException 
     */
    public void runServer() throws IOException {
        // opens Socket, closing it afterwards
    	try (ServerSocket listenAudio = new ServerSocket(11433)) {
            try (ServerSocket listenChat = new ServerSocket(11434)) {

                while (true) {
                	// waiting for clients to connect
                    Socket audioSocket = listenAudio.accept();
                    Socket chatSocket = listenChat.accept();
                    // put new client into the list
                    clientList.add(new ConnectedClients(chatSocket));
                    // start thread for last client
                    clientList.get(clientList.size()-1).start();
                    // starts to send audio to client
                    new Sound(audioSocket).start(); 
                    // enables chatting
                    new Chat().start();

                }
            }
        }
    }

    /**
     * Send parts of audiofile to client, until client stops
     * Is thread, so client also can chat with others
     * @author niels
     *
     */
    class Sound extends Thread {
        Socket socket;
        // TODO: Documentation
        // TODO: Comments

        /**
         * Initialize Audiosocket
         * @param s Socket for audio
         */
        public Sound(Socket s) {
            socket = s;
        }
        
        
        public void run() {
            System.out.println("Opened connection to new client on Port: " + socket.getLocalPort());
            FileInputStream audioFile = null;
            PrintStream clientOut = null;
            BufferedReader clientIn = null;

            try {
            	// reads audio from file 
                audioFile = new FileInputStream("../09 Die Sprache der Dummen.wav");
                // take socket streams
                clientOut = new PrintStream(socket.getOutputStream());
                clientIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // value to stop endless loop
                Boolean stillAlive = true;
                String message;
                while (stillAlive) {
                    // bytes from audio file
                	byte[] audioBytes = new byte[1024];
                	int numBytesRead = 0;
                	// as long as there are bytes to read, send it to client
                    while ((numBytesRead = audioFile.read(audioBytes)) != -1) {
                        clientOut.write(audioBytes);
                    }
                    try {
                    	// should I continue to send data? 
                        message = clientIn.readLine();
                    } catch(Exception ex) {
                    	// client is not there are anymore
                        System.out.println("A client has disconnected");
                        stillAlive = false;
                    }
                }
                // closing all streams that were opened
                audioFile.close();
                clientIn.close();
                clientOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    
    /**
     * passing messages to connected clients
     * @author niels
     *
     */

    class Chat extends Thread {

        public Chat() {

        }
        /**
         * tries to take an item from the queue an writes it to the clients
         */
        public void run() {
            while (true) {
                try {
                    // take newest message
                    String message = messages.take();
                    // write to all clients
                    for (ConnectedClients client : clientList) {
                        client.write(message);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    
    /**
     * Handle behaviour of individual Clients
     * reads and write into Socket-Streams 
     * @author niels
     *
     */
    class ConnectedClients extends Thread {

        BufferedReader inputStream;
        PrintStream outputStream;
        Socket socket;
        
        /**
         * initializes Global values 
         * @param socket socket for chatting
         * @throws IOException when connecting to socket is not possible
         */
        public ConnectedClients(Socket socket) throws IOException {
            this.socket = socket;

            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outputStream = new PrintStream(socket.getOutputStream());
        }

        /**
         * waits for input from Client and puts it into the queue
         */
        public void run() {
            while (true) {
                try {
                    String message = inputStream.readLine();
                    messages.put(message);
                } catch(NullPointerException ex) {
                  return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Prints message into Socket
         * @param message message to print
         */
        public void write(String message) {
            outputStream.println(message);
        }

    }
}
