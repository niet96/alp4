import java.io.*;
import java.net.*;

/**
 * WebRadio Class
 */
public class Server {
    /**
     * Listens to port and accept every connection try
     * @param args not used
     * @throws IOException if no socket is available
     */
    public static void main(String args[]) throws IOException {
        try(ServerSocket listen = new ServerSocket(11433)){
            while (true) {
                Socket s = listen.accept();
                new Service(s).start(); // new thread for new client
            }
        }
    }


    /**
     * Service to send sound to client
     */
    static class Service extends Thread {
        Socket socket;

        /**
         * Constructor
         * @param s audiosocket
         */
        Service(Socket s) {
            socket = s;
        }

        /**
         * Reads file and writes to client via socket
         */
        public void run() {
            System.out.println("Opened connection to new client on Port: " + socket.getLocalPort());
            FileInputStream audioFile = null;
            PrintStream clientOut = null;
            BufferedReader clientIn = null;

            try {
                // read audio file
                audioFile = new FileInputStream("09 Die Sprache der Dummen.wav");
                // use input and output stream
                clientOut = new PrintStream(socket.getOutputStream());
                clientIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // to end loop
                Boolean stillAlive = true;
                String message;
                while (stillAlive) {
                    System.out.println("entered endless loop");
                    // bytes from audiofile
                    byte[] audioBytes = new byte[1024];
                    int numBytesRead;
                    while ((numBytesRead = audioFile.read(audioBytes)) != -1) {
                        clientOut.write(audioBytes);
                    }
                    // should we continue
                    message = clientIn.readLine();
                    if (message == null ) {
                        stillAlive = false;
                        break;
                    }
                    
                }
                // Closing all connections
                audioFile.close();
                clientIn.close();
                clientOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}