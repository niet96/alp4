import java.io.*;
import java.net.*;
import java.util.Scanner;

import javax.sound.sampled.*;

public class Client {
    public static void main (String args[]) throws IOException{
        // inititalize socket and its streams
        Socket socket = new Socket("localhost", 11433);
        PrintStream serverOut = new PrintStream(socket.getOutputStream());
        InputStream inputStream = new BufferedInputStream(socket.getInputStream());
        // Scanner for commandline input
        Scanner keyboard = new Scanner(System.in);
        
        try {
            // "translate" incomming stream to audiostream
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputStream);
            int bytesPerFrame = audioInputStream.getFormat().getFrameSize();
            
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioInputStream.getFormat());
            // set audio output line
            SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
            // bytes from audiofile
            byte[] audiobytes = new byte[1024];
            int numBytesRead = 0;
            // open start audio output
            sourceLine.open();
            sourceLine.start();
            String message;
            while(true) {
                // read as long as there is something to read
                while ((numBytesRead = audioInputStream.read(audiobytes)) != -1) {
                    sourceLine.write(audiobytes, 0, audiobytes.length);
                }
                // should we continue
                message = keyboard.nextLine();
                if (message.equalsIgnoreCase("stop")) {
                    serverOut.println("stop");
                    break;
                } else {
                    serverOut.println("weiter");
                }                
            }
            // close audio output
            sourceLine.drain();
            sourceLine.stop();
            sourceLine.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close all connections to server
        inputStream.close(); 
        serverOut.close();
        socket.close();
    }
}
