// compile: gcc -Wall -Werror -Wextra -pedantic -lpthread -lrt -std=gnu11 aufgabe2.c -o aufgabe2
// ausführung ./aufgabe2 number_producer number_consumer 
// Vorlage vorlesung 8 folie 22 f

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

//#define NUM_PLACES      3
int NUM_PLACES; // buffer size
int NUM_THREADS;
sem_t mutex, empty, full, producing; // semaphores
int last = 0; // index of last element
int *buffer;

void* Producer (void *threadid) {
  int error = 0;
  for (int i = 0; i < 1000; i++) {
    sem_wait(&full); // only if there is space left go further
    error = sem_trywait(&full);
    if(error){
      sem_wait(&mutex); // enter critical section
      NUM_PLACES++;
      buffer = realloc(buffer, NUM_PLACES*sizeof(int)); // increase size of buffer
      sem_post(&full); // create a semaphore more
      sem_post(&mutex);
    }
    sem_wait(&mutex); // i am the only one working
    buffer[last] = i; // deliver products
    printf("Thread %ld (Producer) puts %d into buffer at place %d \n", (long) threadid, buffer[last], last);
    last++; // go to next spot
    sem_post(&mutex); // maybe i am not the only one
    sem_post(&empty); // not empty (anymore)
  }
  sem_wait(&producing);
  pthread_exit(NULL);
}

void* Consumer (void *threadid) {
  int producing_threads; // "current" value of semaphore
  int error; // error message trywait
  sem_getvalue(&producing, &producing_threads); // write current value of producing into producing threads
  while(producing_threads != 0){ // as long as someone produces
    error = sem_trywait(&empty); // is it the gdr ?
    if(!error){ // if everything is fine
      sem_wait(&mutex); // let me consume
      while(last > 0){ // to avoid negative buffer index
        last--; // nothing left
        printf ("Thread %ld (Consumer) takes %d from place %d \n", (long) threadid, buffer[last], last);
        sem_post(&full); // now there is more space to fill
      }
      sem_post(&mutex); // let other consume
    }
    sem_getvalue(&producing, &producing_threads); // refresh value
  }
  pthread_exit(NULL);
}


int main(int argc, char const *argv[])
{
  if(argc !=3){ // check if enough arguemntes
    printf("\n To many or not enoguh arguments needs to be exactly 2 \n");
    exit(-1);
  }
  int num_producer = strtol(argv[1], NULL, 10); // cast cl arguemnts to long
  int num_comsumer = strtol(argv[2], NULL, 10);
  // source for above https://stackoverflow.com/a/9748402
  if(num_producer < 1 || num_comsumer < 1){ // prevent some stupid input
    printf("There must be at least one producer and one consumer\n");
    exit(-1);
  }

  NUM_PLACES = num_producer; // every producer has it "own" space
  buffer = (int *) calloc(NUM_PLACES, sizeof(int)); // initialize buffer
  NUM_THREADS = num_producer + num_comsumer; // producer + consumer

  pthread_t threads[NUM_THREADS]; // thread array
  int rc; // thread return code
  long t; // thread ids
  // initialize semaphores
  sem_init(&mutex, 0, 1);
  sem_init(&empty, 0, 0);
  sem_init(&full, 0, NUM_PLACES);
  sem_init(&producing, 0, num_producer);
  /*
  for (t = 0; t < NUM_PLACES; t++) {
    buffer[t] = 0; // no production yet
  }*/
  for (t = 0; t < NUM_THREADS; t++) { // creating threads
    if (t < num_producer){
      rc = pthread_create (&threads[t], NULL, Producer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
    }
    else {
      rc = pthread_create (&threads[t], NULL, Consumer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
    }
  }
  for (t = 0; t < NUM_THREADS; t++) { // join threads
    pthread_join (threads[t], NULL);
  }
  // cleaning up
  sem_destroy(&full);
  sem_destroy(&empty);
  sem_destroy(&mutex);
  sem_destroy(&producing);
  free(buffer);
  printf("Simulation ended\n");
  return 0;
}
