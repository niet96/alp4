// compile: gcc -Wall -Werror -Werror -pedantic -lpthread -std=gnu11 aufgabe1.c -o aufgabe1
// ausführung ./aufgabe1
// quelle vl 6 folie 8f
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>

//ein Auto wird erstellt
pthread_mutex_t _lock;
int number_of_cars; // number of threads
int bridge_blocked = 0; // counter for cars on the bridge


//ein Auto wird erstellt
void *car (void *threadid) {
  long index = (long) threadid;
  int _error;
 // wait time
  struct timespec time = {0};
  time.tv_nsec = 5;
  time.tv_sec = 0;

  _error = pthread_mutex_lock(&_lock); // try to enter the critical section
  printf("in car%ld: auf der Brücke\n", index);
  bridge_blocked++; // number of cars on the bridge
  nanosleep(&time, (struct timespec *) NULL); // source: http://timmurphy.org/2009/09/29/nanosleep-in-c-c/

  if (bridge_blocked >= 2) { // two cars are crashing
     printf("in car%ld: ///Crash///\n", index);
     bridge_blocked = bridge_blocked - 1; // cleaning up the bridge
     _error = pthread_mutex_unlock(&_lock);  // return from critical section
     if(_error){ //errorhandling for mutex
        printf("mutex error number: %i\n", _error);
        exit(-1);
     }
     pthread_exit (NULL);
  }
  bridge_blocked--; // cleaning up the bridge
  printf("in car%ld: --- hat Brücke verlassen ---\n", index);
  _error = pthread_mutex_unlock(&_lock);  // return from critical section
  if(_error){ //errorhandling for mutex
        printf("mutex error number: %i\n", _error);
        exit(-1);
     }
  pthread_exit (NULL);

}

/////////////////////////MAIN/////////////////////////////////
int main (int argc, char *argv[]) {
  printf("\nWie viele Autos sollen unterwegs sein? ");
  scanf("%d", &number_of_cars); // read input from stdin

  pthread_t threads[number_of_cars]; // thread array

  int rc; // return code
  long t; // thread_id

  pthread_mutex_init(&_lock, NULL); // initialize lock
  for (int j = 0; j <= 100000; j++) { // crossing the bridge
    printf("\nAnzahl Autos: %d \n", number_of_cars);

    // init data
    for (t = 0; t < number_of_cars; t++) {
      printf ("in main: car%ld wird erstellt\n", t);
      rc = pthread_create (&threads[t], NULL, car, (void *)t); // create new thread
      if (rc) { // falls was anderes als NULL zurueck kommt -> fehler
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }

    // wait for each thread to complete
    for (t = 0; t < number_of_cars; t ++) {
      rc = pthread_join(threads[t], NULL);
      if (rc) { // falls was anderes als NULL zurueck kommt
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  }

  printf("\nin main: Autos fuhren 100.000 über die Brücke\n\n");
  /* Last thing that main() should do */
  pthread_exit (NULL);
}
