// gcc -Wall -Werror -Wextra -pedantic -lm -std=gnu11 aufgabe2.c -o aufgabe2 -fopenmp -lrt
// export OMP_NUM_THREADS=4
// ./aufgabe2

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <omp.h>
#include <time.h>

#define NUM_BODY  256 // Anzahl der Koerper
#define BILLION 1000000000L

double norm3(double x, double y) { // Hilfsfunktion zur Norm
  return pow(sqrt(pow(x,2)+pow(y,2)),3);
}

int main(void) {
  uint64_t diff; // Zeitunterschied
  double diff_s; // Zeitunterschied in s
  struct timespec start, end;

  double G = 6.6743; // Gravitationskonstante, skaliert
  double x_values[NUM_BODY]; // x-Werte der Koerper
  double y_values[NUM_BODY]; // y-Werte der Koerper
  double masses[NUM_BODY]; // Massen der Koerper
  double vel_x[NUM_BODY]; // Geschwindigkeiten der Koerper in  x-Richtung
  double vel_y[NUM_BODY]; // Geschwindigkeiten der Koerper in  y-Richtung
  double acc_x[NUM_BODY]; // Bewegung in x-Richtung
  double acc_y[NUM_BODY]; // Bewegung in y-Richtung

  for (int l = 0; l < NUM_BODY; l++) { // Initialisierung mit geeigneten Werten
    x_values[l] = l % 16;
    y_values[l] = (int) (l/16);
    vel_x[l] = 0;
    vel_y[l] = 0;
    masses[l] = pow((x_values[l] - 7.5),2) + pow((y_values[l] - 7.5),2);
  }

  struct timespec ts;
  timespec_get(&ts, TIME_UTC);
  clock_gettime(CLOCK_MONOTONIC, &start); // bekomme aktuelle Zeit

  for (int n = 0; n < NUM_BODY; n++) { // Anfangszustand ausgeben
    printf("K%d: x=%f, y=%f \n", n, x_values[n], y_values[n]);
  }

  int j, t;
  double dist, koeff;

  for (int i = 0; i < 1001; i++) {
    for (j = 0; j < NUM_BODY; j++) { // momentane Bewegung initialisieren
      acc_x[j] = 0;
      acc_y[j] = 0;
    }

    #pragma omp parallel shared (x_values, y_values, masses, acc_x, acc_y, vel_x, vel_y) private (j, t, dist, koeff)
    {
      #pragma omp for schedule (static)
      for (j = 0; j < NUM_BODY; j++) { // Fuer jeden Koerper ...
        for (t = 0; t < NUM_BODY; t++) { // Interferenz mit anderen Koerpern betrachten
          if (t != j) {
            dist = norm3(x_values[j]-x_values[t],y_values[j]-y_values[t]); // Distanz der momentan betrachteten Koerper
            koeff = G *masses[t]/dist; // Koeffizient der Bewegung
            acc_x[j] = acc_x[j] + koeff*(x_values[t]-x_values[j]); // Bewegungsaenderung in x durch zweiten Koerper berechnen
            acc_y[j] = acc_y[j] + koeff*(y_values[t]-y_values[j]); // Bewegungsaenderung in y durch zweiten Koerper berechnen
          }
        }
      }
      #pragma omp for schedule (static)
      for (j = 0; j < NUM_BODY; j++) {
        vel_x[j] = vel_x[j] + acc_x[j]; // Berechnen der Geschwindigkeitsaenderung
        vel_y[j] = vel_y[j] + acc_y[j];
        x_values[j] = x_values[j] + vel_x[j]; // Durchfuehren der errechneten Bewegung in x
        y_values[j] = y_values[j] + vel_y[j]; // Durchfuehren der errechneten Bewegung in y
      }
    }
    if (i%100 == 0) { //printe nur jeden hundersten Schritt
      printf("ITERATION %d\n", i);
      for (int m = 0; m < NUM_BODY; m++) { // momentanen Zustand ausgeben
        printf("K%d: x=%f, y=%f \n", m, x_values[m], y_values[m]);
      }
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &end); // bekomme aktuelle Zeit
  diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec; // berechne Differenz
  diff_s = (double) diff / BILLION;
  printf("\nelapsed time = %f seconds\n", diff_s);
  //Quelle für Zeitmessung: https://www.cs.rutgers.edu/~pxk/416/notes/c-tutorials/gettime.html
}
