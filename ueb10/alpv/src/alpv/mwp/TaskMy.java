package alpv.mwp;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TaskMy implements Task<Integer, Integer>, Serializable {

    @Override
    public Integer exec(Integer a) {

        return a + 1;
    }

}