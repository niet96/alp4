package alpv.mwp;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;

@SuppressWarnings("serial")
public class JobMy implements Job<Integer, Integer, List<Integer>>, Serializable {
    Task<Integer, Integer> myTask = new TaskMy();
    RemoteFutureMy remFuture = new RemoteFutureMy();

    List<Integer> arguments;
    Integer argCount;

    public List<Integer> getArguments() {
        return arguments;
    }

    public void setArguments(List<Integer> arguments) {
        this.arguments = arguments;
    }

    @Override
    public Task<Integer, Integer> getTask() {
        return myTask;
    }

    @Override
    public RemoteFuture<List<Integer>> getFuture() {
        return remFuture;
    }

    @Override
    public void split(Pool<Integer> argPool, int workerCount) {
        argCount = arguments.size();
        for (Integer arg : arguments) {
            try {
                argPool.put(arg);
            } catch (RemoteException e) {
            }
        }
    }

    @Override
    public void merge(Pool<Integer> resPool) {
        while(argCount > 0) {
            Integer temp = null;
            try {
                temp = resPool.get();
            } catch (Exception e) {
            }
            if(temp == null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                continue;
            }
            remFuture.putIntoReturnObject(temp);
            argCount--;
        }
        synchronized(remFuture) {
            remFuture.notify();
        }
    }

}
