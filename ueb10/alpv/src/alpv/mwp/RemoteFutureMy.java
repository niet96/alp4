package alpv.mwp;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;

@SuppressWarnings("serial")
public class RemoteFutureMy implements RemoteFuture<List<Integer>>, Serializable {
    List<Integer> retObj;

    @Override
    public List<Integer> get() throws RemoteException {
        synchronized(this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
            return retObj;
        }
    }

    public void putIntoReturnObject(Integer x) {
        synchronized(this) {
            retObj.add(x);
        }
    }

}