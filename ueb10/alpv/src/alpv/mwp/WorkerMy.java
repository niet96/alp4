package alpv.mwp;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class WorkerMy implements Worker, Serializable {

    public static void main(String[] args) {
        String host = args[0];
        Integer port = Integer.parseInt(args[1]);
        Integer numThread = Integer.parseInt(args[2]);

        new WorkerMy(host, port, numThread);
        synchronized (host) {
            try {
                host.wait();
            } catch (InterruptedException e) {
            }
        }
    }

    private List<Assignment> assignments = new LinkedList<>();
    private Integer numThread;

    public WorkerMy(String host, Integer port, Integer numThread) {
        try {
            Registry registry = LocateRegistry.getRegistry(host, port);
            Master myMaster = (Master) registry.lookup("carrot");
            myMaster.registerWorker(this);
            this.numThread = numThread;
            new Thread(new ThreadManager()).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public <Argument, Result> void start(Task<Argument, Result> t, Pool<Argument> argpool, Pool<Result> respool) throws RemoteException {
        synchronized (assignments) {
            assignments.add (new Assignment(t, argpool, respool));
            if (assignments.size() == 1) {
                assignments.notify();
            }
        }
    }

    class ThreadManager implements Runnable {
        @Override
        public void run() {
            Assignment currentAssignment;
            while (true) {
                try {
                    synchronized (assignments) {
                        if (assignments.size() == 0) {
                            assignments.wait();
                        }
                        currentAssignment = assignments.get(0);
                    }
                    Integer assignmentSize = currentAssignment.getInPool().size();

                    while (assignmentSize > 0) {
                        synchronized (numThread) {
                            if (numThread == 0) {
                                numThread.wait();
                                assignmentSize = currentAssignment.getInPool().size();
                            }
                            for (int i = 0; i < Math.min(assignmentSize, numThread); i++) {
                                new Thread(new ComputingThread(currentAssignment)).start();
                                numThread--;
                            }
                        }
                        assignmentSize = currentAssignment.getInPool().size();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class ComputingThread implements Runnable {
        Assignment assignment;

        public ComputingThread(Assignment assignment) {
            this.assignment = assignment;
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        @Override
        public void run() {
            Task t;
            Object in;
            Object out;

            try {
                synchronized (assignment) {
                    in = assignment.getInPool().get();
                    t = assignment.getTask();
                }
                out = t.exec(in);
                synchronized (assignment) {
                    assignment.getOutPool().put(out);
                }
                synchronized (numThread) {
                    numThread++;
                    numThread.notify();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("rawtypes")
    class Assignment {
        Task task;
        Pool inPool;
        Pool outPool;

        public Assignment(Task task, Pool inPool, Pool outPool) {
            this.task = task;
            this.inPool = inPool;
            this.outPool = outPool;
        }

        public Task getTask() {
            return task;
        }

        public Pool getInPool() {
            return inPool;
        }

        public Pool getOutPool() {
            return outPool;
        }
    }
}
