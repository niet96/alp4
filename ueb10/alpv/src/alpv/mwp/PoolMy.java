package alpv.mwp;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class PoolMy<T> implements Pool<T>, Serializable {

    protected LinkedList<T> list;

    public PoolMy(){
        list = new LinkedList<>();
    }

    @Override
    public synchronized void put(T t) throws RemoteException{
        list.add(t);
    }

    @Override
    public synchronized T get() throws RemoteException{
        return list.get(0);
    }

    @Override
    public synchronized int size() throws RemoteException{
        return list.size();
    }
}
