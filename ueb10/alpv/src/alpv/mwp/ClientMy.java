package alpv.mwp;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.List;

public class ClientMy {

    public static void main(String[] args) {
        String host = args[0];
        Integer port = Integer.parseInt(args[1]);

        try {
            Registry registry = LocateRegistry.getRegistry(host, port);
            Server myServer = (Server) registry.lookup("carrot");

            JobMy job = new JobMy();
            LinkedList<Integer> numbers = new LinkedList<>();
            for(int i = 0; i < 10; i++) {
                numbers.add(i);
            }
            job.setArguments(numbers);

            RemoteFutureMy remFuture = (RemoteFutureMy) myServer.doJob(job);
            List<Integer> result = remFuture.get();

            System.out.print("|");
            for (Integer x : result) {
                System.out.print(x + "|");
            }
            System.out.print("\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}