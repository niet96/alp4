package alpv.mwp;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class MasterServerMy extends UnicastRemoteObject implements Master, Server {

    public static void main(String[] args) {
        Integer port = Integer.parseInt(args[0]);
        try {
            new MasterServerMy(port);
        } catch (RemoteException e) {
        }
        synchronized (port) {
            try {
                port.wait();
            } catch (InterruptedException e) {
            }
        }
    }

    LinkedList<Worker> workers = new LinkedList<>();

    public MasterServerMy(Integer port) throws RemoteException {
        try {
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("carrot", this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public <Argument, Result, ReturnObject> RemoteFuture<ReturnObject> doJob(Job<Argument, Result, ReturnObject> job) throws RemoteException {
        Pool<Argument> argPool = new PoolMy<>();
        Pool<Result> resPool = new PoolMy<>();
        Integer workerCount;
        synchronized (workers) {
            workerCount = workers.size();
        }

        job.split(argPool, workerCount);
        RemoteFuture<ReturnObject> result = job.getFuture();

        new Thread(new Merger(resPool, job)).start();
        new Thread(new WorkerStarter(job.getTask(), argPool, resPool)).start();

        return result;
    }

    @Override
    public void registerWorker(Worker w) throws RemoteException {
        synchronized (workers) {
            workers.add(w);
        }
    }

    @Override
    public void unregisterWorker(Worker w) throws RemoteException {
        synchronized (workers) {
            workers.remove(w);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    class Merger implements Runnable {
        Pool resPool;
        Job job;

        public Merger(Pool resPool, Job job) {
            this.resPool = resPool;
            this.job = job;
        }

        @Override
        public void run() {
            job.merge(resPool);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    class WorkerStarter implements Runnable {
        Task task;
        Pool argPool;
        Pool resPool;

        public WorkerStarter(Task task, Pool argPool, Pool resPool) {
            this.task = task;
            this.argPool = argPool;
            this.resPool = resPool;
        }

        @Override
        public void run() {
            synchronized (workers) {
                for (Worker w : workers) {
                    try {
                        w.start(task, argPool, resPool);
                    } catch (RemoteException e) {
                    }
                }
            }
        }
    }
}
