package alpv.implementation;


import alpv.mwp.*;


import javax.swing.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class MasterImple implements Master, Server {

    ArrayList<Worker> currentWorkers;


    public MasterImple(int port) throws RemoteException, AlreadyBoundException{
        currentWorkers = new ArrayList<Worker>();
        Remote stub = UnicastRemoteObject.exportObject(this, port);
        Job job = (Job) stub;
        Registry registry = LocateRegistry.getRegistry();
        registry.bind("Hello", (Remote) stub);

        System.out.println("Server ready");
    }

    public static void main(String[] args) {
    /*
        arg[0] port
     */
        try{
            if (args.length == 1){
                MasterImple master = new MasterImple(Integer.parseInt(args[0]));

            }else{
                System.out.println("Standardeinstellungen werden benutzt");
                // seen in Oracle Docs
                MasterImple master = new MasterImple(0);
            }
        }catch (AlreadyBoundException ex){
            System.err.println("Server exception: " + ex.toString());
            ex.printStackTrace();
        } catch (RemoteException ex) {
            System.err.println("Server exception: " + ex.toString());
            ex.printStackTrace();
        }

    }


    /**
     * called by a worker to register itself to the master
     * @param worker
     * @throws RemoteException
     */
    @Override
    public void registerWorker(Worker worker) throws RemoteException{
        System.out.println("Arbeiter wird registriert");
        currentWorkers.add(worker);
        System.out.println("Aktuell sind " + currentWorkers.size() + " Arbeiter registriert");

    }

    /**
     * called by a worker to unregister itself or by the master to remove unresponsive worker
     * @param worker
     * @throws RemoteException
     */
    @Override
    public void unregisterWorker(Worker worker) throws RemoteException{
        System.out.println("Arbeiterr wird aus der Registry entfernt");
        currentWorkers.remove(worker);
        System.out.println("Es befinden sich noch "+  currentWorkers.size() + " in der Registry");
    }

    @Override
    public <Argument, Result, ReturnObject> RemoteFuture<ReturnObject> doJob(Job<Argument, Result, ReturnObject> j) throws RemoteException {
        // TODO: Implemnet
        return null;
    }
}
