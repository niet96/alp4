package alpv.implementation;

import alpv.mwp.Pool;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class PoolImple<T> implements Pool<T> {

    Semaphore available;
    public int size; // current size of pool
    ArrayList<T> pool; // actual pool


    public  PoolImple(){
        size = 0; // nothing in the pool at the beginning
        available = new Semaphore(1); // allows
        pool = new ArrayList<T>();
    }

    /**
     * deposit t in the pool
     * @param t
     * @throws RemoteException
     */
    @Override
    public void put(T t) throws RemoteException{

        try {
            available.acquire();
            pool.add(t);
            available.release();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }

    }


    /**
     * retrieve the first element from the pool or null if there is none
     * @return
     * @throws RemoteException
     */
    @Override
    public T get() throws RemoteException{

        T object = null;

        try {
            available.acquire();
            object = pool.get(0);
            available.release();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
        return object;
    }


    /**
     * returns the number of items in the pool
     * @return
     * @throws RemoteException
     */
    @Override
    public int size() throws RemoteException{

        return size;
    }
}
