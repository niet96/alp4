### Nachbesprechung und Hinweise:
* Sobald man mehr Prozesse hat als Prozessoren, gilt: Mutex hat bessere Prozessorauslastung, deshalb
 ist es besser als busy waiting (peterson) 
* Bei Mutex hat man eine Warteschlange, deshalb fair.
* Formaler Beweis wird klausurrelevant.

<img src="/uploads/c50b155650b24e6b7702d1e5441d3fe8/1.jpg" width="600">

### Vorbesprechung:
* Aufgabe 3: Folie 8 Seite 43 für die Bewertung nutzen

### Informelle Definitionen:
* Semaphore: Warteschlange wo in den kritischen Abschnitt mehrere Threads reinkommen
* Monitor: Das was ein Semaphor ist plus Sicherungen bestimmter Codeabschnitte
* Verklemmung: sie warten nicht
* Verhungern: nach endlich viel Zeit kommt er trotzdem nicht ran
* sowohl Semaphore als auch Monitore sind Objekte