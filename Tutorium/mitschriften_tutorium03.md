###Tutorium 03

#### Nachbesprechung Uebung 1

##### Aufgabe 1

###### Korrektheit 
- Syntax durch Definition unseres Modells (Prog Sprache)
- Semantik entspricht Programmspezifikation
- Terminierung
- Determinismus
- korrekte Reihenfolge
- Programmiermodell wird in korrektes in Ausführungsmodell

##### Aufgabe 2
compiler flags: 
- --pedantic
- --Wall
- -Wextra
- -Werror

##### Aufgabe 3

#### Wiederholung
##### Petri-Netz
- Beispiel (nicht kopiert)
- doppelpfeile bilden reuse ab
- beschriftung der kanten geben anzahl an (default: 1)
- beschriftung der knoten geben max. anzahl an (default: 1)
- benutzt für verteilte systeme

##### Peterson-Algorithmus (beweis)
T_0
```
while True:
a   
    t_0 = True
b
    p = 1
c 
    while t_1 and p =1:
        [wait]
d        //KA
    t_0 = False
```
T_1
```
while True:
A
    t_1 = True
B
    p = 0
C
    while t_0 and p = 0:
        [wait]
D        //KA
    t_1 = False
```
Hinweis: & entspricht dem logischen und (latex befehl \land)
1) Es können nicht T_0 und T_1 gleichzeit verhungern
    Beweis durch Widerspruch.
    Angenommen: T_0 und T_1 verhungern
    => Irgendwann hängen T_0 und T_1 in c bzw. C fest
    => (t_1 & p = 1) & (t_0 & p =0)
    => Verklemmungsfreiheit

2)  Kein einzelner kann verhungern
    Beweis durch Widerspruch.
    Angenommen: T_0 verhungert
    Springe zum Zeitpunkt, wo T_0 das letzte mal c betritt.
    Springe zum Zeitpunkt danach, wo T_1 C betritt.
    Warum kann T_1 überhaupt wieder nach C? (Wegen 1)
    p = 0 & t_1 & t_0 (t_0 weil in c)
    p = 0 & t_0 gilt jetzt _immer_ 
    T_1 kommt nie weiter 

#### Vorbesprechung
- Aufgabe 1 
- Aufgabe 2 : was macht mutex so toll (warum?)? 
- z.b. mutex arbeitet mit 0S zusammen 
- Aufgabe 3:
  - schleife machen
  - s. wiederholung