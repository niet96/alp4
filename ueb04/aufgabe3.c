// compile: gcc -Wall -Werror -Wextra -pedantic -lpthread -std=gnu11 aufgabe3.c -o aufgabe3
// ausführung ./aufgabe3
//inspiriert von https://docs.oracle.com/cd/E19205-01/820-0619/gepji/index.html

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

int number_of_philospher;
bool *free_chopsticks;
pthread_mutex_t *chopsticks; //array, wo die jeweiligen chopsticks nur von einem thread beansprucht werden können
int *food; //Array, pro Eintrag steht Zahl wie viel ein Philo essen möchte

//ein Philosph wird erstellt
void *philosophers (void *threadid) {
  long index = (long) threadid;
  int left_chopstick, right_chopstick; // position of the chopsticks
  right_chopstick = index; //Stäbchen die zur Verfügung stehen
  left_chopstick = index + 1;
  int _error_left; // error code for left chopstick
  int _error_right; // error code for right chopstick
  struct timespec time = {0}; // wait time for everything(think, eat, etc)
  time.tv_nsec = 3; // 1 nanosecond
  time.tv_sec = 0; // zero seconds
  if (left_chopstick == number_of_philospher){
    left_chopstick = 0;
  }

  for (int i = 0; i < 20; i++) { //wie in der VL 8
    food[index] = 5;
    while (food[index] > 0) { //sie essen, denken und essen dann wieder

      printf ("Philosopher %ld: denkt für %ld ns\n", index, time.tv_nsec);
      nanosleep(&time, (struct timespec *) NULL); // think

      _error_right = pthread_mutex_trylock(&chopsticks[right_chopstick]); // try to grab right stick

      if(_error_right == 0){ // if everything went fine

        printf ("Philosopher %ld: hat rechtes Stäbchen %d \n", index, right_chopstick);
        _error_left = pthread_mutex_trylock(&chopsticks[left_chopstick]); // try to grab left stick

        if(_error_left == 0){ // if everything went fine
          printf ("Philosopher %ld: hat linkes Stäbchen %d \n", index, left_chopstick);
          // eating
          printf("Philosopher %ld: isst für %ld ns\n", index, time.tv_nsec);
          nanosleep(&time, (struct timespec *) NULL); // actual eating
          food[index] = food[index] -1;
          pthread_mutex_unlock(&chopsticks[left_chopstick]); // drop left chopstick
        }
        pthread_mutex_unlock(&chopsticks[right_chopstick]); // drop right chopstick
      }
    }
  }
  printf ("Philosopher %ld: ist komplett fertig mit Essen.\n \n", index);
  pthread_exit(NULL);
}

/////////////////////////MAIN/////////////////////////////////
int main (void) {
  int rc;
  printf("\nWie viele Philosophen soll es geben? ");
  scanf("%d", &number_of_philospher); // lese eingabe vom terminal
  pthread_t philo[number_of_philospher]; // thread array
  chopsticks = (pthread_mutex_t*) malloc (number_of_philospher * sizeof(pthread_mutex_t)); // lock for every chopstick
  food = (int*) malloc ((number_of_philospher) * sizeof(int)); // number of plates
  free_chopsticks = (bool*) malloc(number_of_philospher * sizeof(bool));

  for (int i = 0; i < number_of_philospher; i++) { // init locks
    pthread_mutex_init (&chopsticks[i], NULL);
  }
  for(int t = 0; t < number_of_philospher; t++) { // generating philosophers
    rc = pthread_create (&philo[t], NULL, philosophers, (void*) (intptr_t) t);
    if (rc) { // falls was anderes als NULL zurueck kommt -> Fehler
      printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
      exit (-1);
    }
  }
  for(int t = 0; t < number_of_philospher; t++) {
    pthread_join (philo[t], NULL);
  }

  free (chopsticks);
  printf("\nAlle Philosophen konnten zuende essen\n");
  return 0;
}
