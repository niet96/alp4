//von den VL Folien!!!!!!!!
// dining philosophers with shared cs
// compile: gcc -Wall -Werror -Wextra -pedantic -lpthread -std=gnu11 test.c -o test
// ausführung ./test
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

#define NUM_THREADS 5
sem_t mutex;

void* philosopher (void *tid) {
  struct timespec time = {0};
  time.tv_nsec = 5;
  time.tv_sec = 0;
  for (int i = 0; i < 1000; i++) {
    // thinking
    nanosleep(&time, (struct timespec*) NULL);
    //wait
    sem_wait(&mutex);
    printf("\n %ld Dining..\n", (long) tid);
    nanosleep(&time, (struct timespec*) NULL);
    printf("\n %ld Finished..\n", (long) tid);
    sem_post(&mutex);
  }
  pthread_exit(NULL);
}

int main (void) {
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  sem_init(&mutex, 0, 2);
  for(t = 0; t < NUM_THREADS; t++) {
    rc = pthread_create (&threads[t], NULL, philosopher, (void *)t);
  }
  if(rc){
    printf("error creating thread, error no. : %d", rc);
  }
  for(t = 0; t < NUM_THREADS; t++) {
    pthread_join (threads[t], NULL);
  }
  sem_destroy(&mutex);
  return 0;
}
