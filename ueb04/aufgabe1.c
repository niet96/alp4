// compile: gcc -Wall -Werror -Wextra -pedantic -lpthread -std=gnu11 aufgabe1.c -o aufgabe1
// ausführung ./aufgabe1
//inspiriert von https://docs.oracle.com/cd/E19205-01/820-0619/gepji/index.html

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>

int number_of_philospher;
pthread_mutex_t *chopstick; //array, wo die jeweiligen chopsticks nur von einem thread beansprucht werden können
int *food; //Array, pro Eintrag steht Zahl wie viel ein Philo essen möchte


//greift nach einem Stäbchen
void grab_chopstick (int phil, char *hand, int chop) {
  pthread_mutex_lock (&chopstick[chop]); // enter critical section
  printf ("Philosopher %d: hat %s Stäbchen %d \n", phil, hand, chop);
}

//lässt beide Stäbchen wieder los
void drop_chopsticks (int chop1, int chop2) {
   pthread_mutex_unlock (&chopstick[chop1]); // drops first chopstick
   pthread_mutex_unlock (&chopstick[chop2]); // drops second chopstick
}

//denkt
void think(int index) {
  struct timespec time = {0}; // defining time needed to think
  time.tv_nsec = (rand() % number_of_philospher) + 1; // random number of nanoseconds
  time.tv_sec = 0; // zero seconds
  printf ("Philosopher %d: denkt für %ld ns\n", index, time.tv_nsec);
  nanosleep(&time, (struct timespec *) NULL); // actual thinking
}

//isst
void eat(int index) {
  struct timespec time = {0}; // defining time needed to eat
  time.tv_nsec = (rand() % number_of_philospher) + 1; // random number of nanoseconds
  time.tv_sec = 0;  // zero seconds
  food[index] = food[index] - 1;
  printf("Philosopher %d: isst für %ld ns\n", index, time.tv_nsec);
  nanosleep(&time, (struct timespec *) NULL); // actual eating
}
//ein Philosph wird erstellt
void *philosophers (void *threadid) {
  long index = (long) threadid;
  int left_chopstick, right_chopstick; // position of the chopsticks
  right_chopstick = index; // Stäbchen die zur Verfügung stehen
  left_chopstick = index + 1;

  if (left_chopstick == number_of_philospher){
    left_chopstick = 0;  // close the circle
  }

  for (int i = 0; i < 20; i++) { //wie in der VL 8
    food[index] = 5;
    while (food[index] > 0) { //sie essen, denken und essen dann wieder
      think(index);
      grab_chopstick (index, "rechtes", right_chopstick); // grab right chopstick
      grab_chopstick (index, "linkes", left_chopstick); // grab right chopstick
      eat(index);
      drop_chopsticks (left_chopstick, right_chopstick);
     }
  }
  printf ("Philosopher %ld: ist komplett fertig mit Essen.\n \n", index);
  pthread_exit(NULL);
}

/////////////////////////MAIN/////////////////////////////////
int main (void) {
  int rc;
  printf("\nWie viele Philosophen soll es geben? ");
  scanf("%d", &number_of_philospher); // lese eingabe vom terminal
  pthread_t philo[number_of_philospher];
  chopstick = (pthread_mutex_t*) malloc (number_of_philospher * sizeof(pthread_mutex_t));
  food = (int*) malloc ((number_of_philospher) * sizeof(int));

  for (int i = 0; i < number_of_philospher; i++) {
    pthread_mutex_init (&chopstick[i], NULL);
  }
  for(int t = 0; t < number_of_philospher; t++) {
    rc = pthread_create (&philo[t], NULL, philosophers, (void*) (intptr_t) t);
    if (rc) { // falls was anderes als NULL zurueck kommt -> Fehler
      printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
      exit (-1);
    }
  }
  for(int t = 0; t < number_of_philospher; t++) {
    pthread_join (philo[t], NULL);
  }

  free (chopstick);
  printf("\nAlle Philosophen konnten zuende essen\n");
  return 0;
}
