// gcc -Wall -Werror -Wextra -pedantic -lpthread -std=gnu11 aufgabe3.c -o aufgabe3
// ./aufgabe3 number_producer number_consumer amount_consume
// Vorlage vorlesung 8 folie 22 f

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#define NUM_PLACES      3 // Buffer Größe
int NUM_THREADS;          // Anzahl threads

typedef struct monitor
{
  int last;  //  Index vom letzten Argument
  int buffer[NUM_PLACES]; // Buffer
  int prod_count; // Anzahl von produzierenden Threads
  pthread_mutex_t mutex;
  pthread_cond_t cond;
} monitor_t;

monitor_t monitor;
int amount_consume;

void iJustDied() {
  pthread_cond_broadcast(&monitor.cond);
}

void ablegen (long threadId, int product) {
  pthread_mutex_lock(&monitor.mutex); // locke zum ablegen
  while (NUM_PLACES == monitor.last) { // schau nach ob noch Platz ist
    pthread_cond_wait(&monitor.cond, &monitor.mutex); // wenn nein, warte und gibt den lock frei
  }
  printf("Thread %ld puts %d into the buffer at place %d\n", threadId, product, monitor.last);
  monitor.buffer[monitor.last] = product; // ansonsten produziere
  monitor.last++;
  pthread_cond_broadcast(&monitor.cond); // wecke alle wartenden Threads auf
  pthread_mutex_unlock(&monitor.mutex); // unlocke mutex
}

void entnehmen (long threadId) {
  pthread_mutex_lock(&monitor.mutex); // locke zum entnehmen
  // schau nach ob was im buffer ist und ob es noch aktive Produzententhread gibt
  // und ob es so viele Gegenstände in der Liste sind, wi der Konsument konsumieren möchte
  while (monitor.last < amount_consume && monitor.prod_count > 0) {
    pthread_cond_wait(&monitor.cond, &monitor.mutex); // wenn nein, warte und gib lock frei
  }
  if (monitor.prod_count > 0) { // wenn es noch aktive Produententhreads gibt
    for (int j = 0; j < amount_consume; j++) { // nehme so viele raus, wie der Konsument nehmen möchte
      monitor.last--;
      printf("Thread %ld takes %d from the buffer at place %d\n", threadId, monitor.buffer[0], 0);
      for (int i = 0; i < NUM_PLACES - 1; i++) {
        monitor.buffer[i] = monitor.buffer[i+1];
      }
    }
  }
  pthread_cond_signal(&monitor.cond); // wecke nur den nächsten thread
  pthread_mutex_unlock(&monitor.mutex);
}

void* Producer (void *threadid) {
  for (int i = (intptr_t) threadid * 1000; i < ((intptr_t) threadid + 1) * 1000; i++) {
    ablegen((long) threadid, i);
  }
  monitor.prod_count--; // jetzt exitiert ein Produzent weniger
  iJustDied(); // wecke alle wartenden Threads auf
  pthread_exit(NULL);
}

void* Consumer (void *threadid) {
  while (monitor.prod_count > 0) { // solange es Produzenten gibt, kann konsumiert werden
    entnehmen((long) threadid); // entnehme aus buffer
  }
  pthread_exit(NULL);
}


int main(int argc, char const *argv[]) {
  if (argc != 4) { // check if enough arguemntes
    printf("\n Too many or not enough arguments needs to be exactly 3 \n");
    exit(-1);
  }
  int num_producer = strtol(argv[1], NULL, 10); // cast cl arguemnts to long
  int num_comsumer = strtol(argv[2], NULL, 10);
  amount_consume = strtol(argv[3], NULL, 10);

  if (num_producer < 1 || num_comsumer < 1) { // prevent some stupid input
    printf("There must be at least one producer and one consumer\n");
    exit(-1);
  }

  if (amount_consume > NUM_PLACES || amount_consume <= 0) {
    printf("Can't consume more than there is and have to consume something.");
    exit(-1);
  }

  NUM_THREADS = num_producer + num_comsumer; // producer + consumer

  pthread_t threads[NUM_THREADS]; // thread array
  int rc; // thread return code
  long t; // thread ids

  monitor.last = 0;
  monitor.prod_count = num_producer;

  if (pthread_mutex_init(&monitor.mutex, NULL) != 0) {
       perror("pthread_mutex_init");
       exit(1);
  }

  if (pthread_cond_init(&monitor.cond, NULL) != 0) {
       perror("pthread_cond_init");
       exit(1);
  }

  for (t = 0; t < NUM_PLACES; t++) { // initialize buffer
    monitor.buffer[t] = 0; // no production yet
  }

  for (t = 0; t < NUM_THREADS; t++) { // creating threads
    if (t < num_producer){
      rc = pthread_create (&threads[t], NULL, Producer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
    }
    else {
      rc = pthread_create (&threads[t], NULL, Consumer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
    }
  }
  for (t = 0; t < NUM_THREADS; t++) { // join threads
    pthread_join (threads[t], NULL);
  }
  // cleaning up
  pthread_cond_destroy(&monitor.cond);
  printf("Simulation ended\n");
  return 0;
}
