// compile: gcc -Wall -Werror -Wextra -pedantic -lpthread -lrt -std=gnu11 aufgabe1.c -o aufgabe1
// ausführung ./aufgabe1 number_producer number_consumer 
// Vorlage vorlesung 8 folie 22 f

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define NUM_PLACES      3 // buffer size
int NUM_THREADS;
sem_t producing;
pthread_mutex_t mutex;

typedef struct monitor
{
  int last;  // index of last element
  sem_t empty;
  sem_t full;
  int buffer[NUM_PLACES]; // buffer
} monitor_t;

monitor_t monitor;

void put(long threadId, int producted){
  

  pthread_mutex_lock(&mutex); // enter critical section
  printf("mutex suceed %ld\n", threadId);
  if(NUM_PLACES - monitor.last){
    printf("waiting %ld\n", threadId);
    sem_wait(&(monitor.full)); // if buffer is full wait here
  }
  printf("Thread %ld puts %d into the buffer at place %d\n", threadId, producted, monitor.last);
  monitor.buffer[monitor.last] = producted; // "real" production
  monitor.last++; // increase buffer iterator
  //pthread_cond_signal(&(monitor.busy)); // let consumer take
  printf("unlocking mutex %ld\n", threadId);
  sem_post(&(monitor.empty)); // buffer not empty anymore
  pthread_mutex_unlock(&mutex); // leave critical section
}

void take(long threadId){
  //int error;
  // sem_wait(&(monitor.empty)); // wait here if buffer is empty
  printf("mutex suceed %ld\n", threadId);
  if(NUM_PLACES - monitor.last == NUM_PLACES ){
    printf("waiting %ld\n", threadId);
    sem_wait(&(monitor.empty));
  }
  pthread_mutex_lock(&mutex);
  printf("Thread %ld takes from the buffer at place %d\n", threadId, monitor.last);
  monitor.last--; // take things from buffer
  printf("unlocking mutex %ld\n", threadId);
  sem_post(&(monitor.full)); // one more thread can put something into the buffer
  pthread_mutex_unlock(&mutex);
}


void* Producer (void *threadid) {
    
  for (int i = 0; i < 1000; i++) {    
    put((long) threadid, i);
  }
  sem_wait(&producing); // take a way producing thread
  pthread_exit(NULL);
}

void* Consumer (void *threadid) {
  // int error; // error message trywait
  int producing_threads;
  sem_getvalue(&producing, &producing_threads);
  while(producing_threads){ // as long as someone produces
    take((long) threadid);
    printf("Consumer %ld took some from the production",(long) threadid);
    sem_getvalue(&producing, &producing_threads); // refresh runtime variable  
  }
  pthread_exit(NULL);
}


int main(int argc, char const *argv[])
{
  if(argc !=3){ // check if enough arguemntes
    printf("\n To many or not enoguh arguments needs to be exactly 2 \n");
    exit(-1);
  }
  int num_producer = strtol(argv[1], NULL, 10); // cast cl arguemnts to long
  int num_comsumer = strtol(argv[2], NULL, 10);
  // source for above https://stackoverflow.com/a/9748402
  if(num_producer < 1 || num_comsumer < 1){ // prevent some stupid input
    printf("There must be at least one producer and one consumer\n");
    exit(-1);
  }

  NUM_THREADS = num_producer + num_comsumer; // producer + consumer

  pthread_t threads[NUM_THREADS]; // thread array
  int rc; // thread return code
  long t; // thread ids

  sem_init(&(monitor.empty), 0, 0);
  sem_init(&(monitor.full),0,NUM_PLACES);
  sem_init(&producing, 0, num_producer);
  
  for (t = 0; t < NUM_PLACES; t++) { // initialize buffer
    monitor.buffer[t] = 0; // no production yet
  }

  for (t = 0; t < NUM_THREADS; t++) { // creating threads
    if (t < num_producer){
      rc = pthread_create (&threads[t], NULL, Producer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
      printf("created producer %ld \n", t);
    }
    else {
      rc = pthread_create (&threads[t], NULL, Consumer, (void *)t);
      if(rc){ // error handling
        printf("in main: Pthread error %d",rc);
      }
      printf("created consumer %ld \n", t);
    }
  }
  for (t = 0; t < NUM_THREADS; t++) { // join threads
    pthread_join (threads[t], NULL);
  }
  // cleaning up
  sem_destroy(&(monitor.empty));
  sem_destroy(&(monitor.full));
  sem_destroy(&producing);
  printf("Simulation ended\n");
  return 0;
}
