import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client{

	public static void main(String[] args){
		Socket server = null;
		Scanner read  = null;
		try{
	    	server = new Socket("localhost", 11000); // opening connection to server 
			read = new Scanner(server.getInputStream()); // scan for messages from server
			for(int i=0;i<3;i++){
	    		System.out.println(read.nextLine()); // read messages from server
	    	}
			read.close(); // close scanner
			server.close(); // close connection to server
	    } catch(UnknownHostException e) {
			e.printStackTrace();
	    } catch(IOException e) {
			e.printStackTrace();
	    }
	}

}
