import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;

public class Server {

	private static void send_messages(Socket client) throws IOException {
		PrintWriter send = new PrintWriter(client.getOutputStream(), true); // creating stream to print to client
		send.println("Hello world.");
		send.println("Hello world.2");
		send.println("super creative message 123");
		send.close(); // closing print outputstream
	}

	public static void main(String[] args) {
		ServerSocket server = new ServerSocket(11000); // creating "connection" socket
		Socket client = null; // declaring "communication socket"
		while (true) {
			try {
				client = server.accept(); // creating connection to client
				send_messages(client); // send messages to client
			} catch (IOException e) {
				System.err.println("Could not send messages to Client");
				e.printStackTrace();
			} finally {
				client.close(); // close connection to client
				server.close(); // shutdown server
			}
			break; // break the endless loop
		}
	}

}
