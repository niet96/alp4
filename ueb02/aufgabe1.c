// compile: gcc -Wall -pedantic -lpthread --std=c11 aufgabe1.c -o aufgabe1
// ausführung ./aufgabe1

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


#define NUM_THREADS 2

int car1, car2 = 0; // no car on the bridge


//um festzustellen ob das Auto auf der Brücke ist
void car_on_the_bridge(long index) {
  if (index == 0) {
    car1 = 1;
  }
  else {
    car2 = 1;
  }
}

//um festzustellen ob das Auto über der Brücke ist
void car_across_the_bridge(long index) {
  if (index == 0) {
    car1 = 0;
  }
  else {
    car2 = 0;
  }
}

//ein Auto wird erstellt
void *car (void *threadid) {
  long index = (long) threadid; //Nr des Threads und gleichzeitig Nr des Autos
  int sleep_time = rand() % NUM_THREADS; //Sekunde wie lange das Auto fährt

  printf("in car%ld: auf der Brücke in %d s\n", index, sleep_time);
  sleep(sleep_time); // ueberquerung der bruecke
  car_on_the_bridge(index);
  printf("in car%ld: Brücke: car0 %d - car1 %d \n", index, car1, car2);
  printf("in car%ld: auf der Brücke für %d s\n", index, 1 + sleep_time);
  sleep(1 + sleep_time);


  if (car1 == 1 && car2 == 1) { // falls beide autos auf der bruecke
     printf("in car%ld: ///Crash///\n", index);
     pthread_exit (NULL);
   }
   car_across_the_bridge(index);
   printf("in car%ld: --- hat Brücke verlassen ---\n", index);
   pthread_exit (NULL);
}

/////////////////////////MAIN/////////////////////////////////
int main (int argc, char *argv[]){

  pthread_t threads[NUM_THREADS];
  int rc; // return code
  long t; // thread id


  for (int j = 0; j <= 100000; j++) { //anstatt 10 - 100.000?
    printf("\n");
    car1 = 0;
    car2 = 0;

    // init data
    for (t = 0; t < NUM_THREADS; t++) { // creating threads
      printf ("in main: car%ld wird erstellt\n", t);
      rc = pthread_create (&threads[t], NULL, car, (void *)t);
      if (rc) { // if thread returned something (else than NULL)
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }

    // wait for each thread to complete
    for (t = 0; t < NUM_THREADS; t ++) {
      rc = pthread_join(threads[t], NULL);
      if (rc) { // if thread returned something (else than NULL)
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  }

  printf("\nin main: Autos fuhren 100.000 über die Brücke\n\n");
  /* Last thing that main() should do */
  pthread_exit (NULL);
}
