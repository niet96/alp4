// compile: gcc -Wall -pedantic -lpthread -std=c11 aufgabe3.c -o aufgabe3
// ausführung ./aufgabe3

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


int *_lock;
int number_of_cars;
int bridge_blocked = 0; // zaehler fuer autos auf der bruecke

int sum_cars(int *(_lock)){
  int sum = 0;
  for (int i = 0; i < number_of_cars; i++) {
    sum = _lock[i] + sum;
  }
  return sum;
}

//ein Auto wird erstellt
void *car (void *threadid) {
  long index = (long) threadid;
  int sleep_time = rand() % number_of_cars; // time needed to cross the bridge

  printf("in car%ld: auf der Brücke in %d s\n", index, sleep_time);
  sleep(sleep_time);

  // try to enter the critical section
  //Quelle VL 4 Folie 36
  _lock[index] = 1;
  while(sum_cars(_lock) > 1) {
    _lock[index] = 0;
    sleep(1);
    _lock[index] = 1;
  }

  printf("in car%ld: auf der Brücke für %d s\n", index, 1 + sleep_time);
  bridge_blocked++; // number of cars on the bridge
  sleep(1 + sleep_time);

  if (bridge_blocked >= 2) { // two cars are crashing
     printf("in car%ld: ///Crash///\n", index);
     bridge_blocked = bridge_blocked - 1; // cleaning up the bridge
     // return from critical section
     _lock[index] = 0;
     pthread_exit (NULL);
  }
  printf("in car%ld: --- hat Brücke verlassen ---\n", index);
  bridge_blocked = bridge_blocked - 1; // decrement/reset counter

  // return from critical section
  _lock[index] = 0;
  pthread_exit (NULL);
}

/////////////////////////MAIN/////////////////////////////////
int main (int argc, char *argv[]) {
  printf("\nWie viele Autos sollen unterwegs sein? ");
  scanf("%d", &number_of_cars); // lese eingabe vom terminal

  pthread_t threads[number_of_cars];
  _lock = (int *)malloc (number_of_cars*sizeof(int));
  int rc; // return code
  long t; // thread_id

  for (int j = 0; j <= 100000; j++) {
    printf("\nAnzahl Autos: %d \n", number_of_cars);

    // init data
    for (t = 0; t < number_of_cars; t++) {
      printf ("in main: car%ld wird erstellt\n", t);
      rc = pthread_create (&threads[t], NULL, car, (void *)t);
      if (rc) { // falls was anderes als NULL zurueck kommt -> fehler
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }

    // wait for each thread to complete
    for (t = 0; t < number_of_cars; t ++) {
      rc = pthread_join(threads[t], NULL);
      if (rc) { // falls was anderes als NULL zurueck kommt
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  }
  free(_lock);
  printf("\nin main: Autos fuhren 100.000 über die Brücke\n\n");
  /* Last thing that main() should do */
  pthread_exit (NULL);
}
