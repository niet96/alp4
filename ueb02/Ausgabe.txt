//Aufgabe 1
in main: car0 wird erstellt
in car0: auf der Brücke in 0 s
in car0: Brücke: car0 1 - car1 0
in car0: auf der Brücke für 1 s
in main: car1 wird erstellt
in car1: auf der Brücke in 1 s
in car0: --- hat Brücke verlassen ---
in car1: Brücke: car0 0 - car1 1
in car1: auf der Brücke für 2 s
in car1: --- hat Brücke verlassen ---

in main: car0 wird erstellt
in main: car1 wird erstellt
in car0: auf der Brücke in 0 s
in car0: Brücke: car0 1 - car1 0
in car0: auf der Brücke für 1 s
in car1: auf der Brücke in 0 s
in car1: Brücke: car0 1 - car1 1
in car1: auf der Brücke für 1 s
in car0: ///Crash///
in car1: ///Crash///
...

//Aufgabe2
in main: car0 wird erstellt
in main: car1 wird erstellt
in car0: auf der Brücke in 1 s
in car1: auf der Brücke in 1 s
in car0: Brücke: car0 1 - car1 0
in car0: auf der Brücke für 2 s
in car0: --- hat Brücke verlassen ---
in car1: Brücke: car0 0 - car1 1
in car1: auf der Brücke für 2 s
in car1: --- hat Brücke verlassen ---

in main: car0 wird erstellt
in main: car1 wird erstellt
in car0: auf der Brücke in 1 s
in car1: auf der Brücke in 0 s
in car1: Brücke: car0 0 - car1 1
in car1: auf der Brücke für 1 s
in car1: --- hat Brücke verlassen ---
in car0: Brücke: car0 1 - car1 0
in car0: auf der Brücke für 2 s
in car0: --- hat Brücke verlassen ---
...

//Aufgabe3
Wie viele Autos sollen unterwegs sein? 4


Anzahl Autos: 4
in main: car0 wird erstellt
in main: car1 wird erstellt
in car0: auf der Brücke in 3 s
in main: car2 wird erstellt
in car1: auf der Brücke in 1 s
in car2: auf der Brücke in 1 s
in main: car3 wird erstellt
in car3: auf der Brücke in 2 s
in car1: auf der Brücke für 2 s
in car1: --- hat Brücke verlassen ---
in car3: auf der Brücke für 3 s
in car3: --- hat Brücke verlassen ---
in car2: auf der Brücke für 2 s
in car2: --- hat Brücke verlassen ---
in car0: auf der Brücke für 4 s
in car0: --- hat Brücke verlassen ---

Anzahl Autos: 4
in main: car0 wird erstellt
in main: car1 wird erstellt
in car0: auf der Brücke in 2 s
in main: car2 wird erstellt
in main: car3 wird erstellt
in car1: auf der Brücke in 0 s
in car2: auf der Brücke in 0 s
in car2: auf der Brücke für 1 s
in car3: auf der Brücke in 2 s
in car2: --- hat Brücke verlassen ---
in car0: auf der Brücke für 3 s
in car0: --- hat Brücke verlassen ---
in car3: auf der Brücke für 3 s
in car3: --- hat Brücke verlassen ---
in car1: auf der Brücke für 1 s
in car1: --- hat Brücke verlassen ---
...
