// compile: gcc -Wall -pedantic -lpthread -std=c11 aufgabe2.c -o aufgabe2
// ausführung ./aufgabe2

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


#define NUM_THREADS 2

int car1, car2 = 0; // keine autos auf der bruecke am anfang
char _lock[2];


//VL 4 Folie 36
int lock(long tid) {
  _lock[tid] = 1;
  while(_lock[NUM_THREADS - 1 - tid]) {
    _lock[tid] = 0;
    sleep(1);
    _lock[tid] = 1;
  }
  return 0;
}

int unlock(long tid) {
  _lock[tid] = 0;
  return 0;
}

//um festzustellen ob das Auto auf der Brücke ist
void car_on_the_bridge(long index){
  if (index == 0) {
    car1 = 1;
  }
  else {
    car2 = 1;
  }
}

//um festzustellen ob das Auto über der Brücke ist
void car_across_the_bridge(long index){
  if (index == 0) {
    car1 = 0;
  }
  else {
    car2 = 0;
  }
}

//ein Auto wird erstellt
void *car (void *threadid) {
  long index = (long) threadid;
  int sleep_time = rand() % NUM_THREADS;

  printf("in car%ld: auf der Brücke in %d s\n", index, sleep_time);
  sleep(sleep_time); // ueberquerung der bruecke

  // try to enter the critical section
  lock(index);
  car_on_the_bridge(index);
  printf("in car%ld: Brücke: car0 %d - car1 %d \n", index, car1, car2);
  printf("in car%ld: auf der Brücke für %d s\n", index, 1 + sleep_time);
  sleep(1 + sleep_time);
  // return from critical section
  unlock(index);
  if (car1 == 1 && car2 == 1) { // beide autos auf der bruecke
     printf("in car%ld: ///Crash///\n", index);
     pthread_exit (NULL);
  }
  car_across_the_bridge(index); // zuruecksetzen der werte
  printf("in car%ld: --- hat Brücke verlassen ---\n", index);
  pthread_exit (NULL);
}

/////////////////////////MAIN/////////////////////////////////
int main (int argc, char *argv[]) {
  pthread_t threads[NUM_THREADS];
  int rc; // return code
  long t; // thread_id

  for (int j = 0; j <= 100000; j++) {
    printf("\n");
    // zuruecksetzen der autos
    car1 = 0;
    car2 = 0;

    // init data
    for (t = 0; t < NUM_THREADS; t++) {
      printf ("in main: car%ld wird erstellt\n", t);
      rc = pthread_create (&threads[t], NULL, car, (void *)t);
      if (rc) { // falls etwas anderes als null zurück gegeben wird -> fehler
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }

    // wait for each thread to complete
    for (t = 0; t < NUM_THREADS; t ++) {
      rc = pthread_join(threads[t], NULL);
      if (rc) { // falls etwas anderes als null zurück gegeben wird -> fehler
        printf ("ERROR IN MAIN; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  }

  printf("\nin main: Autos fuhren 100.000 über die Brücke\n\n");
  /* Last thing that main() should do */
  pthread_exit (NULL);
}
